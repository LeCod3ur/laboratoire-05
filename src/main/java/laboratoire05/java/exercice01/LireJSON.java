package laboratoire05.java.exercice01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class LireJSON {
    public static void main(String[] args){
        try {
            FileReader lecture = new FileReader("src/main/resources/compte.json");
            JSONTokener tokener = new JSONTokener(lecture);
            JSONObject compte = new JSONObject(tokener);
            String numero = compte.getString("numero");
            String type = compte.getString("type");
            double solde = compte.getDouble("solde");
            String devise = compte.getString("devise");
            System.out.println(numero + " " + type + " " + solde + " " + devise);
        } catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        } catch (JSONException ex){
            System.out.println(ex);
        }
    }
}
