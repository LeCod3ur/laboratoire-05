package laboratoire05.java.exercice02;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class LireListeCompte {
    public static void main (String[] args){
        try {
            FileReader lecture = new FileReader("src/main/resources/listeCompte.json");
            JSONTokener tokener = new JSONTokener(lecture);
            JSONObject root = new JSONObject(tokener);
            JSONArray listeCompte = root.getJSONArray("listeCompte");
            for (int i=0; i<listeCompte.length(); i++){
                JSONObject compte = listeCompte.getJSONObject(i);
                String numero = compte.getString("Numero");
                String type = compte.getString("Type");
                double solde = compte.getDouble("Solde");
                String devise = compte.getString("Devise");
                System.out.println(numero + " " + type + " " + solde + " " + devise);
            }
        } catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        catch (JSONException ex) {
            System.out.println(ex);
        }
    }
}
