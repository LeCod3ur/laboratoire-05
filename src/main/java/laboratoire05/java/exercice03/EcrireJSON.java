package laboratoire05.java.exercice03;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class EcrireJSON {

    public static void main(String [] args){
        try {
            JSONObject compte = new JSONObject();
            compte.put("Numero", "CO-0002");
            compte.put("Type", "Epargne");
            compte.put("Solde", 8000);
            compte.put("Devise", "USD");

            //JSON to file: "JSONCompte.json"
            PrintWriter ecriture = new PrintWriter("src/main/resources/compte1.json");
            ecriture.write(compte.toString());

            ecriture.flush();
            ecriture.close();
            System.out.println("Enregistrement Fini - Voir le document dans le fichier resource");
        } catch (JSONException ex){
            System.out.println(ex.getMessage());
        } catch (FileNotFoundException ex){
            System.out.println(ex);
        }
    }
}
