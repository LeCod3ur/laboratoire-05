package laboratoire05.java.exercice04;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class EcrireListeCompteJSON {
    public static void main(String[] args){
        try {
            JSONArray listeCompte = new JSONArray();
            JSONObject compte = new JSONObject();
            compte.put("Numero", "CO-0001");
            compte.put("Type", "Epargne");
            compte.put("Solde", 4000);
            compte.put("Devise", "CAD");
            listeCompte.put(compte);

            compte = new JSONObject();
            compte.put("Numero", "CO-0002");
            compte.put("Type", "CELI");
            compte.put("Solde", 6000);
            compte.put("Devise", "EURO");
            listeCompte.put(compte);

            compte = new JSONObject();
            compte.put("Numero", "CO-0003");
            compte.put("Type", "CHEQUE");
            compte.put("Solde", 10000);
            compte.put("Devise", "USD");
            listeCompte.put(compte);

            JSONObject tableauCompte = new JSONObject();
            tableauCompte.put("listeCompte", listeCompte);

            PrintWriter ecriture = new PrintWriter("src/main/resources/listeCompte1.json");
            ecriture.write(tableauCompte.toString());

            ecriture.flush();
            ecriture.close();
            System.out.println("Enregistrement fini - le fichier dans le dossier resources");
        } catch (JSONException ex){
            System.out.println(ex.getMessage());
        } catch (FileNotFoundException ex){
            System.out.println(ex);
        }
    }
}
