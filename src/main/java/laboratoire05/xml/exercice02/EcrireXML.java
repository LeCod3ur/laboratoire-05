package laboratoire05.xml.exercice02;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class EcrireXML {

    public static void main(String[] args){

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try{
            //creation d'un parseur
            final DocumentBuilder builder = factory.newDocumentBuilder();

            //creation du document
            final Document document = builder.newDocument();

            //creation des elements a la racine
            final Element racine = document.createElement("repertoire");
            document.appendChild(racine);

            //creation d'une personne
            final Comment commentaire = document.createComment("John DOE");
            racine.appendChild(commentaire);

            final Element personne = document.createElement("personne");
            personne.setAttribute("sexe", "masculin");
            racine.appendChild(personne);

            //creation de nom et prenom
            final Element nom = document.createElement("nom");
            nom.appendChild(document.createTextNode("Doe"));

            final Element prenom = document.createElement("prenom");
            prenom.appendChild(document.createTextNode("John"));

            personne.appendChild(nom);
            personne.appendChild(prenom);

            //creation des numeros de telephone
            final Element telephones = document.createElement("telephones");

            final Element fixe = document.createElement("telephone");
            fixe.appendChild(document.createTextNode("514-043-5783"));
            fixe.setAttribute("type", "fixe");

            final Element cell = document.createElement("telephone");
            cell.appendChild(document.createTextNode("438-432-6576"));
            cell.setAttribute("type", "cell");

            //affcher
            final TransformerFactory transformerFactory = TransformerFactory.newInstance();
            final Transformer transformer = transformerFactory.newTransformer();
            final DOMSource source = new DOMSource(document);
            final StreamResult sortie = new StreamResult(new File("src/main/resources/repertoire2.xml"));

            //Prologue
            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

            //Format
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            //sortie
            transformer.transform(source, sortie);
            System.out.println("Enregistrement fini - le fichier est dans le dossier resources");

        } catch (final ParserConfigurationException e){
            e.printStackTrace();
        } catch (TransformerConfigurationException e){
            e.printStackTrace();
        } catch (TransformerException e){
            e.printStackTrace();
        }
    }
}
