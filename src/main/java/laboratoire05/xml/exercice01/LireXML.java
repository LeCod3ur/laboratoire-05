package laboratoire05.xml.exercice01;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class LireXML {

    public static void main(String[] args){
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try{
            //creation d'un parseur
            final DocumentBuilder builder = factory.newDocumentBuilder();

            //creation du document
            final Document document = builder.parse(new File("src/main/resources/repertoire.xml"));

            //affichage du prologue
            System.out.println("********************PROLOGUE********************");
            System.out.println("version: " + document.getXmlVersion());
            System.out.println("encodage :" + document.getXmlEncoding());
            System.out.println("standalone :" + document.getXmlStandalone());

            //recuperer les element racine
            final Element racine = document.getDocumentElement();

            //Affichage de l'element
            System.out.println("\n********************RACINE********************");
            System.out.println(racine.getNodeName());

            //recupere les personnes
            final NodeList racineNoeuds = racine.getChildNodes();
            final int nbRacineNoeuds = racineNoeuds.getLength();

            for (int i=0; i<nbRacineNoeuds; i++){
                if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE){
                    final Element personne = (Element) racineNoeuds.item(i);

                    //afficher de personnes
                    System.out.println("\n********************PERSONNE********************");
                    System.out.println("sexe : " + personne.getAttribute("sexe"));

                    //recupere le nom et prenom
                    final Element nom = (Element) personne.getElementsByTagName("nom").item(0);
                    final Element prenom = (Element) personne.getElementsByTagName("prenom").item(0);

                    //afficher le nom et prenom
                    System.out.println("nom : " + nom.getTextContent());
                    System.out.println("prenom : " + prenom.getTextContent());

                    //recupere le numero de tel
                    final NodeList telephones = personne.getElementsByTagName("telephone");
                    final int nbTelephonesElements = telephones.getLength();

                    for (int j=0; j<nbTelephonesElements; j++){
                        final Element telephone = (Element) telephones.item(j);

                        //Affiche le numero de tel
                        System.out.println(telephone.getAttribute("type") + " : " + telephone.getTextContent());
                    }

                }
            }

        } catch (final ParserConfigurationException e) {
            e.printStackTrace();
        } catch (final SAXException e){
            e.printStackTrace();
        } catch (final IOException e){
            e.printStackTrace();
        }
    }
}
